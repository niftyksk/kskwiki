# Blueprint to Billions

## Recommendation

Since 1980, only 387 U.S. companies - including Microsoft, Nike, Staples, Siebel Systems and Harley Davidson - have achieved the extraordinary, exponential growth it takes to earn revenues of more than $1 billion. Author David G. Thomson calls these exemplars "Blueprint Companies" and he views the key elements of their shared "success-based patterns" as a schematic other architects of business can follow as they build. His book, based on three years of study, research and interviews with the leaders of these billion-dollar companies, is a useful analysis of seven concrete entrepreneurial characteristics that these organizations have in common. Thomson explains what companies must do to rise like these skyscrapers. If you want to shape the economic skyline, _getAbstract_ thinks you will find it very enlightening to see just how hard you’ll have to work, and what the leaders in the billion-dollar club had to accomplish on their way up.

## Take-Aways

-   The 387 U.S. "Blueprint Companies" that have achieved billion-dollar annual revenues since 1980 share seven "essential" traits that drive exponential growth:
-   One: Create a "breakthrough" customer-driven value proposition.
-   Two: Identify a "high-growth market," which can come from many sectors, such as retail or consumer products, not just high tech.
-   Three: Market to passionate customers to generate powerful earnings. You can’t grow alone.
-   Four: Create "Big Brother" business affiliations to open fresh markets.
-   Five: Command the right mix of investment and return for exponential growth.
-   Six: Put a pair of sharp executives in charge. A balanced duo makes the best "inside-outside" management team.
-   Seven: Fill your board with virtuosos who know your industry in depth.
-   Innovation drives exponential growth, so your environment must foster innovation.
-   Management values must be consistent, even during chaotic growth.

## Summary

### From Zero to Hero

"Blueprint Companies" demonstrate a growth "trajectory" that soars beyond $1 billion in revenue. The U.S. gives birth to about 31 such billion-dollar earners annually; the 387 blueprint companies in this study have gone public since 1980. They typically follow a common, discernible growth pattern. It begins with a preparatory "runway" period of gathering strength, setting a pace and preparing for rapid growth. That leads to an "inflection point," when revenues shoot up exponentially, doubling annually. Then, over varying lengths of time, blueprint companies grow from their inflection points to $1 billion in revenue. To foster this phenomenon in your company, replicate the "seven essentials to achieve exponential growth."

### "Essential #1: Create and Sustain a Breakthrough Value Proposition"

A company achieves a breakthrough value proposition when it delivers a product or service that fulfills an eager, specific market demand. This value proposition has three critical components:

1.  The market - Build value by creating new markets or refining your existing value proposition. Genentech, a biotech company, created a new market with innovative pharmaceutical products. You can also craft a breakthrough value proposition by providing a service that solves a key problem in an inventive way.
2.  The customer - Blueprint companies do not focus only on their products or services. They focus on their customers. People-focused companies, such as Dell, know their customers and can put a human face on their market.
3.  The benefits - To achieve superior "customer benefits," move beyond the basic fulfillment of a market need. Surpass the status quo. Climb the ladder of value with a product or service that gives customers "emotional" as well as "functional" benefits. JetBlue, which creates a desirable customer experience for a reduced cost, transcends the functional benefit of its service to connect with customers on a higher level.

> “Blueprint companies represent 5% of American companies that went public since 1980 and account for 56% of employment in 2005 and 64% of market value!”

Additionally, companies that reach $1 billion in revenue find, generate or capture three different kinds of markets to exploit their value propositions:

1.  "Shapers of a New World" - These companies radically altered the environment to invent fresh markets. For example, Yahoo!, eBay and Genentech created markets from scratch with new products and services that never before existed. Shapers must be truly innovative and must address an unmet need. They tend to continue to innovate, instead of resting on their laurels. Ultimately, they become the "de facto standard" in their new markets.
2.  "Niche shapers" - These companies redefine the market by refining an existing value proposition. Starbucks, for instance, fundamentally changed the market for coffee.
3.  "Category killers" - These companies, such as Home Depot, dramatically improve extant products or services, for example, by cutting costs and increasing quality, thus shutting off competition.

### "Essential #2: Exploit a High-Growth Market Segment"

Blueprint companies identify markets with huge potential and pursue them. Their hottest opportunities reside in the largest American markets, although these markets are already highly developed. Contrary to popular perception, these success stories are not just pharmaceutical or high-technology companies. In fact, a broad variety of industries and economic sectors produce companies with $1 billion revenues.

> “Despite the diversity of corporate histories in different economic sectors, the seven common essentials rose to the surface again and again.”

The "retail specialty store" sector produces many, including Autozone and Autonation in auto parts and auto sales; Staples and Office Depot in office supplies; and Williams-Sonoma and Pottery Barn in home goods. Fewer technology companies attain blueprint status, but they are likelier to generate the highest average value when they reach that level. Blueprint companies use insights about their customers to exploit high-growth markets. In fact, they ingrain customer insight into their corporate cultures. Such companies often generate products in conjunction with their suppliers, thus lowering costs and creating differentiated products. These firms typically exploit multiple market segments, not just one area.

### "Essential #3: Marquee Customers Shape the Revenue Powerhouse"

A "marquee customer" is more than just a consumer. Marquee customers help shape a company by "testing and deploying" new products or services, and generating useful responses. As you build your "customer-centric value proposition" and create increasingly close relationships with marquee customers, they can provide valuable insight. They also create momentum and sales by recommending your products to other customers. Each group of marquee customers embraces innovation at its own speed:

1.  "First Movers" - These customers are edgy "thought leaders" who seek innovative new ideas. They are comfortable taking big risks on unproven products or services.
2.  "Fast Followers" - Not quite on the vanguard, these customers require a bit more security before they commit to a new service or product. They follow first movers.
3.  "Mainstream" - These typical customers avoid risk. They only adopt products or services with proven records.
4.  "Utilitarians" - These customers are the last group to step off the dock. They want references, case studies and results. They seek security.

### "Essential #4: Leverage Big Brother Alliances for Breaking into New Markets"

Alliances often shape blueprint companies, which establish partnerships that generate mutually beneficial, strategic "Big Brother-Little Brother" relationships. These alliances open new markets and additional avenues for reaching customers. One prime example is the way IBM initially acted as a big brother to nurture Microsoft. The software company got its start by solving IBM’s need for a new operating system for its PCs. Blueprint companies pursue four kinds of alliances:

1.  "Product alliances to secure a customer beachhead" - Such pairings provide access to customers whom the companies could not reach otherwise.
2.  "Mindshare alliances" - Relationships need synergy, like the connection between Starbucks and T-Mobile, its Wi-Fi partner. This mutual boost can create the kind of higher-order customer benefits that build value for both companies. Starbucks’ brand appeal includes the customer perk of high-speed Internet access in its cafés.
3.  "Marquee customer alliances" - These sustained alliances generate a volume of good customers through cooperative, profitable relationships that reach into linked markets.
4.  "Alliances to maximize customer life-cycle revenues" - To achieve billion-dollar revenue growth, companies often must build some portion of their business outside of their typical customer interactions. Starbucks raised revenues by forming partnerships with other companies to create new distribution channels for its products.

### "Essential #5: Become the Masters of Exponential Returns"

Blueprint companies exercise expert financial control, striking just the right balance between shareholder returns and investment. To make your organization a blueprint company, put as much distance as possible between the earnings on the capital your company invests and the amount your company spends attaining that capital. Expertise in this area means mastering cash flow and gross margins, as well as containing expenses, to produce the highest value possible. Obviously, the management team’s duty is to deliver "on earnings, cash flow and return on equity" at the most sophisticated level. Companies must meet three goals to create the value that results in billion-dollar earnings:

1.  "High spread of return on invested capital above the cost of capital" - Once you master this rule, the best way to invest profits is to reinvest in the company.
2.  "Revenue and revenue growth rate" - To build blistering revenues, the company’s investments must deliver a higher return than the norm.
3.  "Sustainability of drivers one and two" - High market share plus high returns is a blueprint for big success.

### "Essential #6: The Management Team - Inside-Outside Leadership"

The business world abounds with "dynamic duos", from Yahoo!’s Jerry Yang and David Filo, to Novell’s Tim Koogle and Jeff Mallett. Such duos run a sizable number of blueprint companies, which typically succeed by teaming an internally focused leader with an externally focused leader. The outside person is often the sales and marketing expert, while the insider is generally the operations expert. Dynamic duos often share an instinctive link, based on mutual esteem, total trust and meshed skills. One is strong where the other is not. These pairs typically succeed as leaders in three crucial areas:

1.  "Focus on relationships and products" - The externally active partner focuses on customers, alliances, investors, the board and other pivotal relationships. The inside expert attends to organizational systems and to product development.
2.  "Drive to innovate and explore" - The leaders balance inventive forward thinking with current problem solving. Often, one person is future-oriented, exploring new opportunities and innovations, while the other maintains the stability born of experience and structure.
3.  "Ability to manage the seven essentials simultaneously" - The leadership pair must be prepared to balance all of the essentials at once to maintain momentum.

> “Leaders who lay out a consistent communications plan and vision - and act with upright values even in crisis - are critical to mobilizing an organization into consistent behaviors.”

Often blueprint companies have a third leader: the "breakaway" innovator. Sometimes this is the company’s founder, the genius who sparks dazzling ideas and creates a whirl of excitement. Meanwhile, consistent, steady members of the management "ecosystem" provide a stable base that allows the innovator to soar. High-growth can create business chaos, but successful managers respond by hewing to core values and planned goals.

### "Essential #7: The Board - Comprised of Essentials Experts"

Successful boards are generally comprised of nine multidisciplinary experts, crucial customers, alliance partners, community leaders and experienced CEOs from other companies. Staples founder Tom Stemberg praised the virtuosos who joined his board as eminent corporate thinkers, from a key investor to the eventual governor of Massachusetts, Mitt Romney, to masterful retail CEO Leo Kahn. Staples’ board, Stemberg says, "knew when growth was too fast and when to diversify. For us, managing growth was like trying to knit on water skis. Trying to be precise to get everything right and moving like a rocket behind the boat."

> “It is a new fact of life: Business-building leaders cannot rely on importing talent anymore - not in a flat world where people can innovate without having to emigrate. Today, in Silicon Valley ’B to B’ and ’B to C’ stand for ’back to Bangalore’ and ’back to China’.”

Your board should include heavyweights in each of the seven critical essentials, although investor-driven boards rarely do as well as boards dominated by subject-area experts. Blueprint CEOs cross-pollinate for strategic strength. Stemberg asked eBay’s Meg Whitman to sit on Staples’ board, and he himself serves on PETsMART’s board.

### "Linking the Seven Essentials"

Exponential growth can be a messy business. Your company must juggle all the balls at once, and master each of the seven essentials to get on the trajectory to $1 billion in revenue. This journey is different for every company, including those with similar strategic assets. Even among the 387 blueprint companies, only 29 have climbed to the $10 billion level, including Nike, Best Buy, Microsoft, Cisco, Oracle, Staples, HCA and Express Scripts. To soar, focus on the seven essentials, particularly your value proposition, a high-growth market, marquee customers and big-brother alliances. Manage your finances for exponential returns. Create the right chemistry on the management team and the board. Your most critical sustaining skill is making the seven essentials work together over the long run. This is crucial given the pressures blueprint companies face, from intense competition to the ever-changing global business environment to the need for individuals to lead balanced personal lives, even within high-flying corporate cultures.

## About the Author

A general management, sales and marketing expert, **David G. Thomson** is a former consultant who has also worked at major companies. He has an MBA from the University of Western Ontario and a degree in electrical engineering from the University of Waterloo.
