
# Atomic Habits - James Clear

## Habits Make Identity

> Best-selling advice guru James Clear offers a workable manual for ending bad habits and creating healthy habits.

Given the booming market in advice relating to business and individual habits, it’s hard to name one advice guru who stands atop the heap – but James Clear is a worthy candidate. With more than a million copies sold of this _New York Times_, _USA Today_ and _Wall Street Journal_ bestseller – one of _Fast Company’s_ Seven Best Business Books of the Year – a website with millions of monthly views, a for-pay newsletter and his business consultancy – The Habits Academy – Clear is a one-man advice industry.

His advice is simple and clear. This is a manual – a practical guide. He embraces a methodical system designed to overcome how today’s media-rich world has shortened the human attention span and left you prey to your own most spurious impulses. You may find a Zen calm by following Clear’s counsel. He advises living in a more conscious way by replacing your less than helpful habits with those that nourish your life.

Glamour.com  wrote, “Clear will show you how to overcome a lack of motivation, change your environment to encourage success, and make time for new (and better) habits.” And Ryan Holiday, author of _Ego is the Enemy_, called it, “A special book that will change how you approach your day and live your life.”

### Small Steps

Clear points out that frequent repetition automates behaviors, turning them into habits. The author argues that most people try to change their habits by listing “what” they want. His alternative to this practice centers on “who” a person wants to become through creating “identity-based habits.”

> Your identity emerges out of your habits. Every action is a vote for the type of person you wish to become.James Clear

For example, people who take pride in their athletic skills will vest in habits that maintain their physical ability and identity as athletes.

### **Changing a Habit**

Clear maintains that daily routines represent an individual’s identity. He reminds you that the quest to change revolves around who you wish to be; making small changes helps you achieve that identity.

### Building Habits

When you encounter a situation, your brain determines how to react. Clear says that when it decides to enact the same behavior repeatedly, the behavior becomes the standard solution in that situation – a habit. Habitual, automated performance decreases your stress and “cognitive load.”

> Genes do not determine your destiny. They determine your areas of opportunity.James Clear

Habits, Clear reveals, follow a four-step process: “Cue, craving, response and reward.” Cues are the activators; cravings are the motivators. Responses are the answers that yield a reward.

Clear structures his advice around “The Four Laws of Behavior Change”:

### “Make It Obvious”

Clear describes how the brain operates by absorbing information, analyzing it and acknowledging repetitive experiences. Thus, he teaches, repeated experiences culminate in a habit, because the brain identifies a recurring situation and reacts in a standardized way.

To eliminate a bad habit, Clear instructs, remove the cues that trigger it. He explains, “It’s easier to avoid temptation than resist it.”

### “Make It Attractive”

Clear details how, when you experience pleasure, your brain’s reward system releases dopamine. This makes you likely to repeat a rewarding experience.

> Desire is the engine that drives behavior.James Clear

But, Clear reveals, when you merely plan to repeat a pleasant action, you get a dopamine hit then, even before you start. Thus, the expectation itself becomes rewarding. That’s why, the author asserts, it’s easier to form a habit when it’s attractive.

### “Make It Easy”

Clear defines a habit as a repetitive behavior you perform so often it becomes automatic. And, he notes, frequency is key. Because the brain seeks to conserve energy, he recounts, it selects options that require the least effort. Choose the path of least resistance. Start small, the author recommends, by engaging in the relevant activity for two minutes. To break a bad habit, make it more difficult to perform.

### “Make It Satisfying”

Behavioral change works through repeating behavior that is “immediately rewarded” and by avoiding behavior that is “immediately punished.” Clear explains that the brain craves quick success, even in small increments. Habits change, he says, when people find the alternatives “attractive, easy and obvious.”

### **Good Habits**

Every behavior requires mastery exercised in small, continuous steps until the activity turns into a good habit. This is bedrock Clear: over time, good habits become mindless, everyday practices.

### Two Minutes

While not offering much innovation, Clear delivers an inspiring pep talk. His enthusiasm makes you want to start right away and put his recipes into practice. Although Clear refers to the science underlying the nature of habits, he focuses on practical guidance and presents useful examples to illustrate its key concepts. He links to further resources on his website, which offers templates and bonus chapters.

> One of our greatest challenges in changing habits is maintaining awareness of what we are actually doing.James Clear

Anyone who has the intention to change something in his or her life – but has struggled to get started – will appreciate Clear’s counsel. It’s encouraging to hear that an investment of as little as two minutes can be the starting point for achieving remarkable change in the long run. And even if you knew this already, it’s reassuring to learn that the little, “atomic” things in life count.

Though the advice world is a crowded field, stand-out ancillary reads include Frank Rivers _The Way of the Owl_ and Don Miguel Ruiz’s _The Four Agreements._ A popular book with a different take on developing organizational habits is _The Life-Changing Magic of Tidying Up_ by Marie Kondo.


