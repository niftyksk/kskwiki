# The Little Book of Stoicism

## Recommendation

Jonas Salzgeber wants you to forget the clichés and distortions  you may associate with Stoicism. In this entertaining and informative text, Salzgeber takes readers on a journey through the Greek  and Roman family tree of this influential and old philosophy. He links ancient and modern examples of mindful-living principles to the original Stoic approach. In the style of the classic Stoic philosophers, Salzgeber provides practical tips for integrating Stoicism into your daily life. A more meaningful, serene existence is possible, he argues, if you learn to develop and flex your Stoic muscles.

## Take-Aways

-   Stoicism offers lessons on the craft of living well.
-   Zeno, a merchant, developed Stoicism in Athens after suffering a shipwreck.
-   The Stoics addressed a key question that Socrates posed: How does a person live a good life?
-   Famous Stoic philosophers include Marcus Aurelius and Seneca.
-   A “Happiness Triangle” stands at the center of Stoic philosophy.
-   Justice, wisdom, self-discipline and courage  are the four cardinal virtues of Stoicism.
-   The legend of William Tell exemplifies Stoicism.
-   Don’t let negative emotions hijack your day, week or life.
-   Develop your Stoic practice.

## Summary

### Stoicism offers lessons on the craft of living well.

How can you become skilled at living? Epictetus, a Stoic philosopher and teacher, offered a simple answer: Practice. Through practice and discipline, you can elevate daily life into an art form. According to the Stoics, moment-to-moment life experiences offer the basic materials for those seeking to master the craft of living well.

> “Every life situation presents a blank canvas or a block of marble that we can sculpt and train on, so that over a lifetime we can master our craft.”

Stoicism provides a manual for creating a good life.  Stoic principles teach you how to calmly face challenges, develop resiliency, enjoy a meaningful existence and excel in the art of living well. The Stoics believed that, through practice, hard work and discipline, you can become the best version of yourself. Outcomes and external conditions don’t matter; your reaction to those events is the main concern. Your negative or positive responses to external events shape the quality of your life.

### Zeno, a merchant, developed Stoicism in Athens after suffering a shipwreck.

In 320 BCE, Zeno, a merchant, lost his entire wealth in a shipwreck off the coast of Greece. After that misfortune, Zeno settled in Athens. There, he wandered into a bookstore and began reading about Socrates, the famous Athenian philosopher.

> “I made a prosperous voyage when I suffered a shipwreck.” (Zeno of Citium)

Impressed with Socrates, Zeno asked the book merchant to suggest other philosophical voices. Instead of recommending a book, the shop owner pointed to the philosopher known as Crates the Cynic, who was walking past. Zeno took the man’s advice and became a long-time follower of Crates. Zeno studied with other philosophers as well, before, ultimately, launching his own brand of philosophy in 301 BCE.

At first, his students called themselves Zenonians, but the name evolved into Stoics. The new name referred to the _Stoa Poikile_ (Painted Porch) – a famous outdoor area in ancient Athens where Zeno gave lectures on philosophy.

### The Stoics addressed a key question that Socrates posed: How does a person live a good life?

Zeno and his early followers’ new philosophy derived from a number of different schools of thought.  Influences included the Cynics, Plato’s Academics and Socrates’s questions – especially those about a life of goodness and value.

> “The Stoics favored a lifestyle that allowed simple comforts. They argued that people should enjoy the good things in life without clinging to them.”

Stoicism flourished and influenced philosophy profoundly for nearly 500 years. A wide array of individuals – rich and poor, powerful and meek – followed Stoic principles. Even after fading from prominence, Stoicism shaped the work of writers and philos0phers for centuries, including the writer Henry David Thoreau and the philosopher and scientist René Descartes.

### Famous Stoic philosophers included Marcus Aurelius and Seneca.

Four major Stoic philosophers from Rome – Seneca, Musonius Rufus, Epictetus and Marcus Aurelius – provided a supporting foundation on which Stoicism bases its principles.

> “Luckily, these brilliant (but also flawed) men did not live in caves somewhere in the mountains, but all of them were fully engaged in society and worked hard to make the world a better place.”

These four individuals were all quite different from one another:

1.  **Lucius Annaeus Seneca the Younger** (4 BCE - 65 CE)– Seneca was a playwright and prolific writer of letters and essays. His surviving work offers key insights into Stoicism. Seneca targeted the practical features of Stoicism, including tips about travel, adversity, wealth, poverty and grief.
2.  **Musonius Rufus** (30 CE - 100 CE)– Rufus believed philosophy should lead to a virtuous life, and he supported gender equality in education. Rufus emphasized universal and practical principles, and provided advice about sex, wardrobe and filial behavior.
3.  **Epictetus** (55 CE - 135 CE) – No one knows his birth name; the term Epictetus means “property,” because he was born a slave. Due to an injury, mistreatment or a birth defect, Epictetus had a crippled leg. His final master – who ultimately freed Epictetus – provided education in Stoicism under the tutelage of Rufus.  Once free, Epictetus launched and operated a school dedicated to Stoic philosophy for almost 25 years. Students from all over the Roman Empire attended the school. Epictetus offered lessons about living with dignity even when confronting difficulties.
4.  **Marcus Aurelius** (121 CE - 180 CE) – Of the major Stoic philosophers, Aurelius is the most famous.  His _Meditations_, a collection of short books, offer insights about self-improvement. He also served as Emperor of Rome.

### A “Happiness Triangle” stands at the center of Stoic philosophy.

In writing and in public speeches, the Stoics offered access to happiness and a life free from strife. By learning and practicing Stoicism, followers can face the challenges that life presents with a calm mind and spirit. The segments of the triangle involve focus, personal responsibility and mindfulness – a here and now search for excellence.

> “Express your highest self in every moment. If we want to be on good terms with our highest self, we need to close the gap between what we’re capable of and what we’re actually doing. This is really about being your best version in the here and now.”

You can apply these principles in your own life. Focus on what you can control. Seize responsibility for your existence.  External factors are beyond your control, but your responses to them are within your control. Pay attention to the moment, and strive to be your best self.

### Justice, wisdom, self-discipline and courage  are the four cardinal virtues of Stoicism.

The leaders of Stoicism crafted four basic virtues that they drew from the Socratic tradition.  These cardinal virtues include justice, wisdom, self-discipline and courage.

> “Virtue really is the highest good in Stoicism and living by it will ultimately shape you into a genuinely good person.”

Justice involves acting with fairness and integrity. Wisdom represents prudent deliberation and the ability to determine a proper course of action. Self-discipline involves inner control and the humility to resist temptation and strong desires. Courage is the capacity to take the correct actions, even in the face of difficult or fear-inducing scenarios.

With courage, you can face life’s inevitable challenges.  You may even come to appreciate hardship.  Consider the legendary challenges that Hercules had to face. By dealing with conflict and difficult tasks, Hercules developed strength. Modern heroes, superstars and sports figures such as tennis star Roger Federer excel by facing struggles. Your challenges offer you opportunities for development and growth.

### The legend of William Tell  exemplifies Stoicism.

Early in the 1300s, Habsburg emperors from Vienna ruled parts of Switzerland.  In one small Swiss town, a governor placed his hat on a pole and ordered all to show respect by bowing when passing the pole. While walking with his son, William Tell failed to bow. As a penalty, the authorities ordered Tell to take his crossbow and aim at an apple sitting on his son’s head. Tell – an expert archer – accurately hit the apple with his arrow.  His calmness under pressure exemplifies Stoicism.

> “We can choose our intentions and actions but the ultimate outcome depends on variables beyond our control.”

Tell’s story highlights the importance of focusing on what you can control (your process), rather than what you can’t control (the outcome).The archer exercised control of his thoughts and behavior, but his control of the final outcome ended once he released the crossbow trigger.  External factors, including the wind, a bird or other people, could have interfered with the arrow’s path.

Do your best, then view the outcome of your activities with detachment. It doesn’t matter if you hit the target or goal. Define success by the excellence and integrity of your efforts and let go of the rest. Calmly accept the factors and outcomes that are beyond your control.

### Don’t let negative emotions hijack your day, week or life.

Negative emotions can override your best instincts and hijack your life. Anger, extreme grief and greed are passions, according to the Stoics, and those negative feelings can hold you hostage. They are the enemies of a good life.

> “Once the enemy has entered the mind, reason is gone. It’s one or the other, reason or passion; when passion is at the steering wheel, reason is tied up and gagged in the trunk.”

When negative emotions seize control, you may find yourself seeking a quick, feel-good solution. Unfortunately, the choice to pursue short-term, pain-relief goals can sabotage your long-term goals.  Your efforts to find an emotional pacifier could involve a long list of harmful actions: marathon television viewing, violence, alcohol abuse, excessive purchases or temper tantrums.

To avoid toxic displays of anger or depression, the Stoics recommend a series of practices that can help calm the mind. Adopt an attitude of awareness in every moment. A mindful stance prompts you to pause, step back and select your best possible response.  Awareness helps you develop self-discipline and wisdom.

### Develop your Stoic practice.

In combat, warriors develop battle skills and muscles through practice.  Likewise, through daily practice you can develop and flex your Stoic muscles.

> “You can’t just hear Stoic principles once and expect to rely on them when life happens. You must practice like a professional athlete and show up on the pitch every day.”

Build your Stoic muscles with the following steps and practices:

-   **Embrace whatever happens** – Don’t fight reality. Battling reality increases your pain.  You can’t select the cards that life deals you, but you can choose how you play those cards.
-   **Accept change** – Nothing is permanent. Marcus Aurelius frequently offered the metaphor of time as a flowing river. This viewpoint prompts you to embrace the moment and appreciate loved ones, who might be swept away tomorrow. Likewise, you yourself will not live forever.  That realization should lead you to live each day more fully. Recognition of constant change diminishes your attachment to material things and reduces your fear of difficulties.
-   **Develop empathy** – Avoid snap judgments by taking the time to consider the difficulties others may be facing, Marcus Aurelius urged.
-   **Be economical with words** – Epictetus recommended economy of speech or vesting in silence. Spend more time observing and listening than talking, and avoid gossip.
-   **Set an example** –  Marcus Aurelius urged his followers to lead by example. Your Stoic actions are more valuable to others than a lecture on Stoicism.

## About the Author

**Jonas Salzgeber** writes for NJlifehacks.com about Stoicism and other topics.

----------

