# The Little Book that Beats the Market

## Recommendation

Whether or not this book’s "magic formula" delivers the results that author Joel Greenblatt promises, the book itself presents a lucid, simple explanation of investing in the stock market. Unlike many who write about investing in stocks and offer formulas for success, Greenblatt is remarkably honest in his discussion of the difficulty of beating the market and remarkably modest in his claims (although perhaps not quite as restrained in referring to his Web site). _getAbstract_ finds that the chief merit of this bestseller is not its formula for success (which derives from guru Benjamin Graham’s value approach), but rather its clear, step-by-step introduction to the fundamentals of investing for novices. The author makes the market understandable to a child. That is quite an achievement.

## Take-Aways

-   Investing in the stock market makes sense.
-   The hard part of investing in stocks is having discipline and perseverance.
-   Most people should not attempt to invest in individual stocks.
-   The best way to invest in stocks is to buy good companies at bargain prices.
-   For investors, good companies are the ones with a high return on capital.
-   When good companies have lower P/E ratios than most companies, they are bargains.
-   Buy good stocks at bargain prices, hold them a year, then sell them and repeat the process.
-   Investors who follow this magic formula may beat the market by a good margin.
-   However, investors need patience and discipline because sometimes the magic formula underperforms the broad market for months or even for years.
-   Make friends with Mr. Market, but watch out: he’s pretty moody.

## Summary

### Chewing Gum Lesson

Jason, who is in the sixth grade, has a very effective business model. He buys several packages of chewing gum for 25 cents each. A pack has five sticks of gum. Jason sells each stick of gum at school for 25 cents, making a $1 profit on each pack. With six years left before he finishes high school, Jason can expect to earn $3,000 in profits before graduation day. But if you wanted to invest in Jason’s company, how would you value it? That is not such a straightforward matter, although thinking about the value of Jason’s gum empire is a way to understand how the stock market works and how a sound investment plan should function. What would you pay for half of Jason’s enterprise? Not $1,500, surely, because that price would merely return your money to you. But why would Jason accept less, since he would stand to earn that much from half of his business?

> “In fact, the story even concludes with a magic formula that can make you rich over time. I kid you not.”

No simple, universally correct answer exists to the question of what Jason’s chewing gum business is worth. Notions of its value depend on the circumstances of the person doing the valuation. Thinking through various possibilities, though, offers a way to understand the stock market a little better. Anyone who buys a stock is buying a share in a business - hence the expression "share" is used as shorthand for an investment in a business.

> “Buying good businesses at bargain prices is the secret to making lots of money.”

Why would you buy shares of stock? The simple answer is to make money. And it’s not a bad answer. Investors buy stocks because they hope to make a profit. That is, in fact, the reason why people usually invest in any type of security. Investors only have four things they can do easily with their money:

1.  Keep it in cash - Also known as putting the money in the mattress. Although cash does not go up or down in nominal value, it does not earn any return. Put $10 in the mattress, leave it for 10 years, and it will still be $10. However, $10 in 10 years will probably not buy as much as $10 today, because inflation erodes the purchasing power of cash over time. Thus, while the nominal value of cash does not change, its real value may indeed go down.
2.  Put the money in a U.S. government-insured bank account - Or buy U.S. government bonds. The U.S. government is sound, and will pay its debts, so the investor in U.S. government bonds or guaranteed instruments earns a stipulated rate of interest and takes no credit risk. The investor does, however, have inflation risk. If the inflation rate rises, the investor who bought bonds may actually lose purchasing power.
3.  Buy non-U.S. government bonds - This includes such things as corporate bonds or the bonds issued by emerging market countries. Such bonds usually pay a higher interest rate, but only to compensate for their additional risk.
4.  Invest in the stock market.

### Investing in the Stock Market

Investors who want to earn more over time than they can expect to get from bonds have to invest in growth. The easiest way to do that is to buy a share of a growing business. Buying a share of a business means to purchase a piece, portion or interest in it. Each investor owns a proportional share of the earnings of the business.

> “It takes a great amount of discipline to save any money.”

Thus, to decide what the stock is worth, you need to arrive at some forecast or estimate (pure guesswork in many cases) of the business’s future earnings. However, the future earnings forecast is not the only element to consider in pricing the stock. Remember that you can earn risk-free returns from U.S. government bonds. If you cannot earn "sufficiently" more than the risk-free rate, it does not make sense to invest in the stock. Whether you think that the return is high enough or not depends on your preferences about risk, but no rational investor takes risk unless the return justifies it. That is, investors demand to be paid to take risk.

### Value versus Price

Stock market prices vary so wildly that they seem to have no relationship to value. Even a company as big as IBM or General Motors might sell its shares for $30 each one day and $60 each a few weeks later. Why? Does IBM suddenly sell twice as many computers? Does GM suddenly sell twice as many cars? Clearly not. Yet over a period of time, any stock can undergo wild price swings. Prices are much more volatile than fundamental value. The business may change little, even though share prices change a lot. The stock price obviously is based on more than the value of the business.

> “You can stick your money under the mattress.” (But that plan kind of stinks.)

A lot of academic work has gone into attempting to explain why such price swings make rational sense. That effort seems quixotic. In fact, a satisfactory rational explanation of stock price moves seems unachievable, although one shorthand explanation is simply that "people just go nuts a lot." Fortunately, investors do not need to understand why some people buy and sell at "wildly different prices over very short periods of time" apparently unrelated to value. You "just have to know that they do!"

> “In effect, the stock market acts very much like a crazy guy named Mr. Market.”

Stock market guru Benjamin Graham imagined that stock market forces had a personality and referred to them as Mr. Market. He described Mr. Market as a moody, inconstant individual who buys and sells shares in businesses. One day, due to his fluctuating moods, Mr. Market might offer to buy or sell at a very low price. On those days, the rational investor would probably take advantage of Mr. Market by buying from him at a low price. On other days, Mr. Market would buy or sell at a very high price. On such days, the rational investor would sell back to Mr. Market what he had bought from him - and pocket a profit. The relevant question for the investor is not why Mr. Market suffers such unpredictable, erratic mood swings, but rather whether to buy or sell from him on any given day. His moods offer opportunities to investors who exercise more discipline and patience than he does.

### The Magic Formula

Before deciding whether to buy or sell, ideally the investor should assess the future earnings potential of the business. Unfortunately, it is extremely difficult to predict the future of anything, especially a business. Fortunately, you can arrive at a reasonably good sense of the quality of a business without knowing the future. Consider the company’s return on capital. This means taking the profits or earnings and dividing them by the capital invested in the business. The beauty of return on capital is that it lets investors compare two unrelated businesses. A doughnut company and a steel company both require capital. Even though doughnuts and steel have nothing else in common, they both have return on their capital. Generally speaking, investors do better in businesses whose return on capital is high, not low.

### How the Formula Works

Paying a bargain price for a business is good for investors. But you also want to buy good businesses - defined as businesses that earn a high return on invested capital. Buying a good business at a bargain price is the "magic formula" for winning in the market, if you add patience.

> “Although over the short term Mr. Market may price stocks based on emotion, over the long term Mr. Market prices stocks based on their value.”

Graham, perhaps the greatest stock market thinker of the twentieth century, created this magic formula. His idea of a bargain was remarkable - it called for purchasing stocks at prices so low that closing the business and selling all of its assets would make a profit for the investor.

However, Graham came up with his formula during the desperate years of the Great Depression. When the economy recovered, few businesses were selling for prices lower than their liquidation value. Today, finding stocks that meet Graham’s strict criteria is rarely, if ever, possible. Fortunately, investors can still earn solid, substantial returns by buying stocks, even though they must pay higher prices than Graham would countenance.

> “We have designed a new magic formula - a formula that seeks to find good companies at bargain prices.”

These days, a bargain price means a price that is comparatively lower than prices for other companies. For the past 20 years or so, investors who bought the 30 stocks that combined a high return on capital with a high-earnings yield would have reaped a return on investment (ROI) of 30% per year. In other words, $11,000 invested according to the magic formula would have turned into $1,000,000 (before taxes and brokerage fees). By comparison, the overall market on average returned slightly more than 12.3% annually during this period.

> “If you just stick to buying good companies (ones that have a high return on capital) and to buying those companies only at bargain prices (at prices that give you a high-earnings yield), you can achieve investment returns that beat the pants off even the best investment professionals.”

However, the magic formula has a downside. It is an excellent long-term strategy, but months and even years can go by during which it seems to be a losing proposition. To use this formula successfully, investors must have faith and perseverance, which most investors do not have - so the formula simply will not work for them.

### Why the Magic Formula Works

Notice that the magic formula combines two factors: high return on capital and high-earnings yield. This formula works for large and small companies. It ranks companies according to earnings yield and return on capital, and assigns a numerical rating to each company. You can find this ranking at www.magicformulainvesting.com. [getAbstract note: Web addresses are subject to change.] Investors buy stock in the companies with the best numerical ratings, hold them a reasonable length of time and sell them.

> “The magic formula works for companies both large and small.”

The magic formula seeks superior companies at bargain prices. Companies with a high return on capital are apt to be good organizations with, most likely, a distinctive competitive edge. For example, Apple’s iPod has advantages in ease of use and market acceptance. Coke’s strong brand attracts more buyers than no-name beverages. eBay was among the earliest Web auction sites, and has more liquidity and more traffic than any other Web site. These companies can invest profits in activities that will continue to earn a high return on capital, merely by reinvesting in what they are already doing. So a higher return on capital can translate fairly easily into a high-earnings growth rate. Businesses without a distinctive advantage are unlikely to give you a superior return. So, merely by focusing on companies that earn a higher return on capital, the magic formula eliminates many poor or mediocre businesses. This should provide some consolation to investors during periods when the magic formula fails to beat the market.

> “For the magic formula to work for you, you must believe that it will work and maintain a long-term investment horizon.”

As noted above, the magic formula sometimes stops working for a while because Mr. Market gets moody and overlooks the real underlying value of companies. However, investors have very little risk of loss over the long term because, although Mr. Market may be irrational in the short term, over the long term Mr. Market does recognize value.

### What It Takes to Use the Magic Formula

When you buy stocks from magic formula companies, phase in your investment, putting about a quarter or a third of your investment amount in at each phase and buying half a dozen or so stocks. Hold each stock for one year, then sell it and use the proceeds to buy another stock according to the magic formula.

> “Companies that achieve a higher return on capital are likely to have a special advantage of some kind.”

**You can select magic formula stocks by checking the Web site noted above or by:**

-   Screening stocks according to return on asset (a proxy for return on capital) and setting the minimum return at 25%.
-   Obtaining a list of the lowest P/E stocks.

When you have your list of potential stocks to buy, delete any of the following: all utilities, all financial industry stocks and all non-U.S. stocks. Also delete any stock with a perversely low P/E, for example, less than five, on the assumption that there is a problem either with the time period or with the data for that company. Also delete any companies that have recently made earnings or other significant announcements, because such announcements distort pricing.

> “On Wall Street, there ain’t no tooth fairy!”

Efficient market theory says that Mr. Market is a rational creature who incorporates information into the prices of stocks. In the long term, Mr. Market does exactly that. However, in the short term, Mr. Market is an erratic, volatile personality. The magic formula lets you turn his volatility to your advantage when you invest.

## About the Author

**Joel Greenblatt** is the founder and a managing partner of a private investment partnership and hedge fund firm. He is an adjunct professor at Columbia University’s business school and the author of _You Can Be a Stock Market Genius_.
